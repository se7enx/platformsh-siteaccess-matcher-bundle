<?php

namespace ContextualCode\PlatformShSiteAccessMatcherBundle\SiteAccess\Matcher\Map;

use eZ\Publish\Core\MVC\Symfony\SiteAccess\Matcher\Map\Host as Base;
use eZ\Publish\Core\MVC\Symfony\Routing\SimplifiedRequest;

class Host extends Base
{
    static $routes = null;

    public function getName()
    {
        return 'host:map';
    }

    /**
     * Injects the request object to match against.
     *
     * @param \eZ\Publish\Core\MVC\Symfony\Routing\SimplifiedRequest $request
     */
    public function setRequest(SimplifiedRequest $request)
    {
        $host = $request->host;
        if (self::$routes === null) {
            self::$routes = getenv('PLATFORM_ROUTES');
            self::$routes = (array) json_decode(base64_decode(self::$routes), true);
            // Remove slash from platform.sh routes
            $tmp = array();
            foreach (self::$routes as $host => $route) {
                $host = rtrim($host, '/');
                $tmp[$host] = $route;
            }
            self::$routes = $tmp;
        }

        if (is_array(self::$routes) && count(self::$routes) > 0 && isset($_SERVER['HTTP_HOST'])) {
            // For cases when we are using platform.sh custom stage domains
            // $request->host will be overridden by forwarded value, but we need to get real HTTP_HOST
            $host = $_SERVER['HTTP_HOST'];
            $key = $request->scheme . '://' . rtrim($host, '/');
            if (is_array(self::$routes) && isset(self::$routes[$key])) {
                $hostData = self::$routes[$key];
                if (isset($hostData['original_url'])) {
                    $originalHost = parse_url(
                        rtrim($hostData['original_url'], '/'),
                        PHP_URL_HOST
                    );
                    if ($originalHost) {
                        $host = $originalHost;
                    }
                }
            }
        }

        if (!$this->key) {
            $this->setMapKey($host);
        }

        parent::setRequest($request);
    }
}
