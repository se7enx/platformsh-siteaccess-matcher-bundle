<?php

namespace ContextualCode\PlatformShSiteAccessMatcherBundle\SiteAccess;

use eZ\Bundle\EzPublishCoreBundle\SiteAccess\Matcher;
use eZ\Bundle\EzPublishCoreBundle\SiteAccess\SiteAccessMatcherRegistryInterface;
use eZ\Publish\Core\MVC\Symfony\Routing\SimplifiedRequest;
use eZ\Publish\Core\MVC\Symfony\SiteAccess\MatcherBuilderInterface;
use eZ\Publish\Core\MVC\Symfony\SiteAccess\MatcherBuilder as BaseMatcherBuilder;
use RuntimeException;

/**
 * Siteaccess matcher builder based on services.
 */
class MatcherBuilder extends BaseMatcherBuilder
{
    /** @var \eZ\Bundle\EzPublishCoreBundle\SiteAccess\SiteAccessMatcherRegistryInterface */
    protected $siteAccessMatcherRegistry;
    /** @var string */
    protected $domainsPrefix;

    public function __construct(
        SiteAccessMatcherRegistryInterface $siteAccessMatcherRegistry,
        ?string $domainsPrefix = null
    ) {
        $this->siteAccessMatcherRegistry = $siteAccessMatcherRegistry;
        $this->domainsPrefix = $domainsPrefix;
    }

    /**
     * Builds siteaccess matcher.
     * If $matchingClass begins with "@", it will be considered as a service identifier and loaded with the service container.
     *
     * @param $matchingClass
     * @param $matchingConfiguration
     * @param \eZ\Publish\Core\MVC\Symfony\Routing\SimplifiedRequest $request
     *
     * @return \eZ\Bundle\EzPublishCoreBundle\SiteAccess\Matcher
     *
     * @throws \RuntimeException
     */
    public function buildMatcher($matchingClass, $matchingConfiguration, SimplifiedRequest $request)
    {
        if ($matchingClass === 'Map\\Host') {
            $matchingClass = "\\" . __NAMESPACE__ . "\\Matcher\\$matchingClass";
        }

        if ($this->domainsPrefix && strpos($request->host, $this->domainsPrefix) === 0) {
            $request->setHost(substr($request->host, strlen($this->domainsPrefix)));
        }

        if (strpos($matchingClass, '@') === 0) {
            $matcher = $this->siteAccessMatcherRegistry->getMatcher(substr($matchingClass, 1));

            $matcher->setMatchingConfiguration($matchingConfiguration);
            $matcher->setRequest($request);

            return $matcher;
        }

        return parent::buildMatcher($matchingClass, $matchingConfiguration, $request);
    }
}
